Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users, only: [:show, :new, :create]
  resource :session, only: [:new, :destroy, :create]
  resources :bands do
    resource :albums, only: [:new]
  end

  resource :albums, except: [:index] do
    resources :tracks, only: [:new]
  end

  resources :tracks, except: [:index]

  root to: redirect('/bands')
end
