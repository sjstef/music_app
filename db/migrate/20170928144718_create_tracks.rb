class CreateTracks < ActiveRecord::Migration[5.1]
  def change
    create_table :tracks do |t|
      t.integer :album_id, null: false
      t.string :title, null: false
      t.integer :track_num, null: false
      t.boolean :bonus
    end
    add_index :tracks, :album_id
  end
end
