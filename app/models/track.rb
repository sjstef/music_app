# == Schema Information
#
# Table name: tracks
#
#  id        :integer          not null, primary key
#  album_id  :integer          not null
#  title     :string           not null
#  track_num :integer          not null
#  bonus     :boolean
#

class Track < ApplicationRecord
  belongs_to :album,
    class_name: 'Album',
    foreign_key: :album_id

  has_one :band,
    through: :album,
    source: :band
    
end
