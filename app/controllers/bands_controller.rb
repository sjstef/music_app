class BandsController < ApplicationController
  before_action :ensure_logged_in

  def new
    @band = Band.new
    render :new
  end

  def index
    render :index
  end

  def ensure_logged_in
    if !logged_in?
      redirect_to new_session_url
    end
  end
end
